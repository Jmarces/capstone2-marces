let token = localStorage.getItem("token");
let adminUser = localStorage.getItem("isAdmin") === "true";
let addButton = document.querySelector("#adminButton");

if(adminUser === false || adminUser === null){

	addButton.innerHTML = null

} else {

	addButton.innerHTML = `

	<div class="col-md-2 offset-md-10">
	<a href="./addCourse.html" class="btn btn-block btn-primary">Add Course</a>
	</div>

	`
}

/*

Activity: 

	use the fetch method to get all the courses and show the data into the console using console.log


*/

fetch('https://warm-hollows-47601.herokuapp.com/api/courses')
  .then(response => response.json())
  .then(data => console.log(data));