let token = localStorage.getItem('token');

let name = document.querySelector("#name");
let email = document.querySelector("#email");
let mobileNo = document.querySelector("#mobileNo");

let courseTable = document.querySelector("#courseTable");

let editProfile = document.querySelector("#editProfile");

fetch(`https://warm-hollows-47601.herokuapp.com/api/users/details`, {
	headers: {
		Authorization: `bearer ${token}`,
	}	
})
.then(res => res.json())
.then(data => {

	console.log(data);

	editProfile.innerHTML =
					`
					<a href="./editProfile.html?userId=${data._id}" class="btn btn-block btn-secondary">Edit Profile</a>
					`

	name.innerHTML = `<strong>Name:</strong>&nbsp&nbsp${data.firstName} ${data.lastName}`
	email.innerHTML = `<strong>Email:</strong>&nbsp&nbsp${data.email}`
	mobileNo.innerHTML = `<strong>Mobile Number:</strong>&nbsp&nbsp${data.mobileNo}`

	// console.log(data.enrollments)

	if(data.enrollments.length < 1) {

		courseTable.innerHTML = 			
		`
		<tr>
		<td colspan="4" ><strong>No Courses Enrolled</strong></td>
		</tr>
		`
	} else {

		let iterator = data.enrollments.values();

		for (let courseList of iterator) {

			// console.log(courseList.courseId);

			fetch(`https://warm-hollows-47601.herokuapp.com/api/courses/${courseList.courseId}`, {})
			.then(res => res.json())
			.then(data => {

				// console.log(data)

				let courseMaster = [];

				let total = courseMaster.push(data)

				// console.log(courseMaster)

				courseTable.innerHTML += courseMaster.map(course => {

					//console.log(course);

					if (course.isActive === true) {
						course.isActive = "Active"
					} else {
						course.isActive = "Inactive"
					}

					return (
						`
						<tr>
							<td>${course.name}</td>
							<td>${courseList.status} since ${courseList.enrolledOn}</td>
							<td>${course.isActive}</td>
						</tr>
						`

						)
				}).join("");
			})
		}
	}
})