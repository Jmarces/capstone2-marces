let token = localStorage.getItem("token");
let adminUser = localStorage.getItem("isAdmin") === "true";
let addButton = document.querySelector("#adminButton");
let cardFooter;

if(adminUser === false || adminUser === null){

	addButton.innerHTML = null

	fetch('https://warm-hollows-47601.herokuapp.com/api/courses')
	.then(res => res.json())
	.then(data => {

		// console.log(data);

		let courseData;

		if(data.length < 1) {
			courseData = "No courses available"
		} else {

			courseData = data.map(course => {

				 console.log(course);

				cardFooter =
				`
				<a href="./course.html?courseId=${course._id}" value={course._id} class="btn btn-success text-white btn-block editButton">
					Select Course
				</a>
				`

				return (
					`
					<div class="col-md-6 my-3">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title">${course.name}</h5>
								<p class="card-text text-left">
									${course.description}
								</p>
								<p class="card-text text-right">
									₱ ${course.price}
								</p> 
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
						</div>
					</div>
					`
					)
			}).join("")
		}

		let container = document.querySelector('#coursesContainer')

		container.innerHTML = courseData;
	})

// Admin Section

} else {

	addButton.innerHTML = `

	<div class="col-md-2 offset-md-10">
		<a href="./addCourse.html" class="btn btn-block btn-primary">Add Course</a>
	</div>

	`

	fetch('https://warm-hollows-47601.herokuapp.com/api/courses/admin')
	.then(res => res.json())
	.then(data => {

		// console.log(data);

		let courseData;

		if(data.length < 1) {
			courseData = "No courses available"
		} else {

			courseData = data.map(course => {

				// console.log(course.isActive);

				if(course.isActive === true) {
					cardFooter =
					`
					<a href="./course.html?courseId=${course._id}" value={course._id} class="btn btn-success text-white btn-block editButton">
						View Course
					</a>
					<a href="./editCourse.html?courseId=${course._id}" value={course._id} class="btn btn-primary text-white btn-block editButton">
						Edit
					</a>
					<a href="./deleteCourse.html?courseId=${course._id}" value={course._id} class="btn btn-danger text-white btn-block deleteButton">
						Archive
					</a>
					`
				} else {
					cardFooter =
					`
					<a href="./course.html?courseId=${course._id}" value={course._id} class="btn btn-success text-white btn-block editButton">
						View Course
					</a>
					<a href="./editCourse.html?courseId=${course._id}" value={course._id} class="btn btn-primary text-white btn-block editButton">
						Edit
					</a>
					<a href="./deleteCourse.html?courseId=${course._id}" value={course._id} class="btn btn-danger text-white btn-block deleteButton">
						Enable
					</a>
					`
				}

				return (
					`
					<div class="col-md-6 my-3">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title">${course.name}</h5>
								<p class="card-text text-left">
									${course.description}
								</p>
								<p class="card-text text-right">
									₱ ${course.price}
								</p> 
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
						</div>
					</div>
					`
					)
			}).join("")
		}

		let container = document.querySelector('#coursesContainer')

		container.innerHTML = courseData;
	})
}