let params = new URLSearchParams(window.location.search)

let courseId = params.get('courseId')

let token = localStorage.getItem('token')

let courseForm = document.querySelector("#editCourse");

let courseName = document.querySelector("#courseName")
let coursePrice = document.querySelector("#coursePrice")
let description = document.querySelector("#courseDescription")

fetch(`https://warm-hollows-47601.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {

	courseName.placeholder = data.name
	coursePrice.placeholder = data.price
	description.placeholder = data.description
	courseName.value = data.name
	coursePrice.value = data.price
	description.value = data.description
})

courseForm.addEventListener("submit", (e) => {

	e.preventDefault()

	let courseName = document.querySelector("#courseName").value
	let coursePrice = document.querySelector("#coursePrice").value;
	let description = document.querySelector("#courseDescription").value;

	if((courseName.length !== 0) && (coursePrice.length !== 0) && (description.length !== 0)){

		fetch('https://warm-hollows-47601.herokuapp.com/api/courses/', {
			method: 'PUT',
			headers: {
				'Authorization': `bearer ${token}`,
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				id: courseId,
				name: courseName,
				description: description,
				price: coursePrice
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true){
				alert("Course Updated")
				window.location.replace("courses.html")

			} else {

				alert("something went wrong")
			}
		})
	} else {
		alert("Please enter valid inputs respectively!")
	}
})