let loginForm = document.querySelector("#logInUser");

loginForm.addEventListener("submit", (e) => {

	e.preventDefault();

	let email = document.querySelector("#userEmail").value;
	let password = document.querySelector("#password").value;

	// console.log(email);
	// console.log(password);

	if (email == "" || password == "") {
		alert("Please input your email and/or password.")
	} else {

		fetch('https://warm-hollows-47601.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: {'Content-Type' : 'application/json'},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res=> res.json())
		.then(data => {

			if(data.accessToken){

				// Set the token in to the local storage
				localStorage.setItem('token', data.accessToken)

				//Note: GET Requests do not NEED its method defined in the options.
				fetch('https://warm-hollows-47601.herokuapp.com/api/users/details', {
					
					headers: {

						Authorization: `Bearer ${data.accessToken}`
					}

				})
				.then(res => res.json())
				.then(data => {

					localStorage.setItem("id", data._id)
					localStorage.setItem("isAdmin", data.isAdmin)

					window.location.replace("./profilePage.html")

				})
			} else {
				alert("Login Failed. Something went wrong.")
			}
		})
	}
})