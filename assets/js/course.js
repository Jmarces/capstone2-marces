let params = new URLSearchParams(window.location.search)

let courseId = params.get('courseId')

let token = localStorage.getItem('token')

let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDesc")
let coursePrice = document.querySelector("#coursePrice")
let enrollContainer = document.querySelector("#enrollContainer")

let adminUser = localStorage.getItem("isAdmin") === "true";

if(adminUser === false || adminUser === null){

	fetch(`https://warm-hollows-47601.herokuapp.com/api/courses/${courseId}`)
	.then(res => res.json())
	.then(data => {

		courseName.innerHTML = data.name
		courseDesc.innerHTML = data.description
		coursePrice.innerHTML = data.price
		enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>`

		document.querySelector("#enrollButton").addEventListener("click", () =>{

			fetch('https://warm-hollows-47601.herokuapp.com/api/users/course-exists', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${token}`
				},
				body: JSON.stringify({
					courseId: courseId
				})
			})
			.then(res => res.json())
			.then(data => {

				if(data === true){

					alert("You have already enrolled this Course!")

				} else {


					fetch('https://warm-hollows-47601.herokuapp.com/api/users/enroll', {

						method: 'POST',
						headers: {
							'Content-Type': 'application/json',
							'Authorization': `Bearer ${token}`
						},
						body: JSON.stringify({
							courseId: courseId
						})
					})
					.then(res => res.json())
					.then(data => {

						if(data === true){

							alert('Thank you for enrolling to the course.')
							window.location.replace('./courses.html')
						} else {

							alert("Register an account to enroll our courses!")
							window.location.replace('./register.html')	
						}
					})
				}
			})
		})
	})

// Admin Section
} else {

	fetch(`https://warm-hollows-47601.herokuapp.com/api/courses/${courseId}`)
	.then(res => res.json())
	.then(data => {

		courseName.innerHTML = data.name
		courseDesc.innerHTML = data.description
		coursePrice.innerHTML = `${data.price}
		<br>
		<br>
		<br>
		<tr>
			<td><Strong><h3>Enrollees:</h3></Strong></td>
		</tr>
			
		`
		
		if(data.enrollees.length < 1) {

			coursePrice.innerHTML = `${data.price}
				<br>
				<br>
				<br>
				<tr>
					<td><Strong><h3>Enrollees:</h3></Strong></td>
				</tr>
				<br>
				<tr>
					<td><Strong><h5>No Enrollees</h5></Strong></td>
				</tr>
			
				`
		} else {

			let iterator = data.enrollees.values();

			for (let studentList of iterator) {

				fetch(`https://warm-hollows-47601.herokuapp.com/api/users/students/${studentList.userId}`, {
				})
				.then(res => res.json())
				.then(data => {

					let studentMaster = [];

					let total = studentMaster.push(data);
					
					coursePrice.innerHTML += studentMaster.map( studentDetails => {

						return (
							`
							<tr>
								<td>${studentDetails.firstName} ${studentDetails.lastName}</td>
							</tr>
							<br>
							`
					    )
					}).join("")
				});
			}
		}	
	})
}