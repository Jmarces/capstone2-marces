let formSubmit = document.querySelector("#createCourse");

formSubmit.addEventListener("submit", (e) => {

	e.preventDefault();

	let courseName = document.querySelector("#courseName").value;
	let description = document.querySelector("#courseDescription").value;
	let price = document.querySelector("#coursePrice").value;

	let token = localStorage.getItem("token")

	if((courseName.length !== 0) && (price.length !== 0) && (description.length !== 0)){

		fetch('https://warm-hollows-47601.herokuapp.com/api/courses', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `bearer ${token}`
			},
			body: JSON.stringify({
				name: courseName,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data === true){

				window.location.replace("./courses.html")

			} else {

				alert("something went wrong")
			}
		})
	} else {
		alert("Please enter valid inputs respectively!")	
	}
})