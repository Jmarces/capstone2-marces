let params = new URLSearchParams(window.location.search)

let userId = params.get('userId')

let token = localStorage.getItem('token')

let registerForm = document.querySelector("#registerUser");

registerForm.addEventListener("submit", (e) => {

	e.preventDefault()

	let firstName = document.querySelector("#firstName").value
	let lastName = document.querySelector("#lastName").value;
	let mobileNo = document.querySelector("#mobileNumber").value;
	let email = document.querySelector("#userEmail").value;
	// let password1 = document.querySelector("#password1").value;
	// let password2 = document.querySelector("#password2").value;

	if(/*(password1 !== '' && password2 !== '' ) && (password1 === password2) &&*/ (mobileNo.length === 11) && (firstName.length !== 0) && (lastName.length !== 0) && (mobileNo !== 0)){

		fetch('https://warm-hollows-47601.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: {'Content-Type' : 'application/json'},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === false) {

				fetch('https://warm-hollows-47601.herokuapp.com/api/users', {
					method: 'PUT',
					headers: {
						'Authorization': `bearer ${token}`,
						'Content-Type': 'application/json',
					},
					body: JSON.stringify({
						id: userId,
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo
						// password: password1
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if(data === true){
						alert("Updated Successfully")

						//redirect to profile page
						window.location.replace("./profilePage.html")
					} else {
						// error in updating
						alert("something went wrong")
					}
				})		
			} else {
				alert("The Email Address provided is already used!")
			}
		})
	} else {
		alert("Please enter valid inputs respectively!")
	}
})
